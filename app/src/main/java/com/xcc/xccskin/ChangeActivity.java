package com.xcc.xccskin;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.xcc.skinlibrary.Logger;
import com.xcc.skinlibrary.OnLoadPluginListener;
import com.xcc.skinlibrary.ResourcesManager;
import com.xcc.skinlibrary.SkinManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class ChangeActivity extends Activity implements View.OnClickListener, OnLoadPluginListener {
    protected void onCreate(Bundle savedInstanceState) {
        SkinManager.getInstance().registerSkin(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.btn);
        button.setOnClickListener(this);

        ResourcesManager manager = SkinManager.getInstance().getResourcesManager();
        if (manager == null) button.setText("更换皮肤");
        else button.setText("使用默认");
    }

    protected void onDestroy() {
        super.onDestroy();
        SkinManager.getInstance().unRegisterSkin(this);
    }

    private static final String apkDataName = "skin1-debug.apk";
    private Button button;

    //仅用于测试
    private void outResName(int resId) {
        Resources resources = getResources();
        String resourceName = resources.getResourceName(resId);
        System.out.println(resourceName);//com.xcc.xccskin:mipmap/ic_launcher

        String typeName = resources.getResourceTypeName(resId);
        System.out.println(typeName);//mipmap
        resourceName = resources.getResourceEntryName(resId);
        System.out.println(resourceName);//ic_launcher

        if ("color".equals(typeName)) {
            int color = resources.getColor(resources.getIdentifier(resourceName, typeName, "com.xcc.xccskin"));
            findViewById(R.id.text).setBackgroundColor(color);
        }
    }

    public void onClick(View v) {
        ResourcesManager manager = SkinManager.getInstance().getResourcesManager();
        if (manager == null) {
            button.setText("使用默认");
            //写文件
            try {
                String dataPath = getFilesDir().getAbsolutePath() + File.separator + apkDataName;
                File file = new File(dataPath);
                if (!file.exists()) {
                    InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("assets/" + apkDataName);
                    FileOutputStream fileOutputStream = openFileOutput(apkDataName, 0);
                    int idx = 0;
                    byte[] buffer = new byte[1024];
                    while ((idx = resourceAsStream.read(buffer)) > 0) {
                        fileOutputStream.write(buffer, 0, idx);
                    }
                    resourceAsStream.close();
                    fileOutputStream.close();
                }
                SkinManager.getInstance().setOnLoadPluginListener(this).changeRes(this, dataPath);
            } catch (Exception e) {
                if (Logger.isDebug) e.printStackTrace();
            }
        } else {
            button.setText("更换皮肤");
            SkinManager.getInstance().changeDefRes(this);
        }
    }

    public void onLoad(ResourcesManager resourcesManager) {
        SkinManager.getInstance().setOnLoadPluginListener(null);
        if (resourcesManager == null) {
            button.setText("默认");
            Toast.makeText(this, "加载失败", Toast.LENGTH_SHORT).show();
        } else {
            button.setText("更换");
            Toast.makeText(this, "加载成功", Toast.LENGTH_SHORT).show();
        }
    }
}
