package com.xcc.xccskin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.xcc.skinlibrary.SkinManager;

/**
 * Created by 52463 on 2018/6/29.
 */

public class MainActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        SkinManager.getInstance().registerSkin(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.btn);
        button.setText("去换皮肤");
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ChangeActivity.class));
            }
        });
    }

    protected void onDestroy() {
        super.onDestroy();
        SkinManager.getInstance().unRegisterSkin(this);
    }
}
