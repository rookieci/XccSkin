package com.xcc.xccskin;

import android.app.Application;

import com.xcc.skinlibrary.SkinManager;

/**
 * Created by 52463 on 2018/6/29.
 */
public class DemoApp extends Application {
    public void onCreate() {
        super.onCreate();
        SkinManager.getInstance().initRes(this);
    }
}
