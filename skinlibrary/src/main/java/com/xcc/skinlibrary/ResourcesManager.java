package com.xcc.skinlibrary;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

/**
 * Created by 52463 on 2018/6/27.
 */
public class ResourcesManager {
    private Resources resources;//插件Resource对象
    private String packageName;//插件Apk的包名

    public ResourcesManager(Resources resources, String packageName) {
        this.resources = resources;
        this.packageName = packageName;
    }

    public Drawable getDrawableByName(String type, String name) {
        try {
            if (TextUtils.isEmpty(type)) type = "drawable";
            return resources.getDrawable(resources.getIdentifier(name, type, packageName));
        } catch (Exception e) {
            return null;
        }
    }

    public ColorStateList getColorByName(String name) {
        try {
            return resources.getColorStateList(resources.getIdentifier(name, "color", packageName));
        } catch (Exception e) {
            return null;
        }
    }
}
