package com.xcc.skinlibrary;

import android.text.TextUtils;
import android.util.Log;

/**
 * Created by xcc on 2016/11/21.
 */
public class Logger {
//    public static final boolean isDebug = false;//BuildConfig.DEBUG
    public static final boolean isDebug = true;

    public static void i(Object object, String msg) {
        if (isDebug) {
            String s = object.toString();
            if (TextUtils.isEmpty(s) || TextUtils.isEmpty(msg))
                return;
            Log.i(s, msg);
        }
    }

    public static void e(Object object, String msg) {
        if (isDebug) {
            String s = object.toString();
            if (TextUtils.isEmpty(s) || TextUtils.isEmpty(msg))
                return;
            Log.e(object.toString(), msg);
        }
    }

    public static void d(Object object, String msg) {
        if (isDebug) {
            String s = object.toString();
            if (TextUtils.isEmpty(s) || TextUtils.isEmpty(msg))
                return;
            Log.d(object.toString(), msg);
        }
    }

    public static void log(String tag, String msg) {
        v(tag, msg);
    }

    public static void v(String tag, String msg) {
        if (isDebug) {
            if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(msg))
                return;
            Log.v(tag, msg);
        }
    }

    public static void out(String msg) {
        if (isDebug && !TextUtils.isEmpty(msg))
            System.out.println(msg);
    }

    public static void println(String msg) {
        out(msg);
    }

    public static void w(String tag, String msg) {
        if (isDebug) {
            if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(msg))
                return;
            Log.w(tag, msg);
        }
    }

    public static void w(String logTag, InterruptedException e) {
        if (isDebug) {
            if (TextUtils.isEmpty(logTag) || e == null)
                return;
            Log.w(logTag, e);
        }
    }
}
