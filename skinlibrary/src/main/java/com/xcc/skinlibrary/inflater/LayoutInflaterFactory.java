package com.xcc.skinlibrary.inflater;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.view.ActionMode;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.xcc.skinlibrary.attrsView.AttrsViewUtils;

/**
 * Created by 52463 on 2018/6/28.
 * 用于创建View
 */
public class LayoutInflaterFactory implements LayoutInflater.Factory2, AppCompatCallback {
    private Activity activity;
    //private AppCompatDelegate appCompatDelegate;
    private MyAppCompatViewInflater appCompatViewInflater;
    private AttrsViewUtils attrsViewUtils;

    public LayoutInflaterFactory(Activity activity) {
        this.activity = activity;
        //使用这个需要activity继承AppCompatActivity，再用getDelegate()获取
        //appCompatDelegate = AppCompatDelegate.create(activity, this);
        appCompatViewInflater = new MyAppCompatViewInflater();
        attrsViewUtils = new AttrsViewUtils(activity);
    }

    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view = null;// appCompatDelegate.createView(parent, name, context, attrs);
        if (view == null) {
            //反射创建View
            view = appCompatViewInflater.createViewFromTag(context, name, attrs);
        }
        if (view != null) attrsViewUtils.addViewAttrs(view, attrs);
        return view;
    }

    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return null;
    }

    public void onSupportActionModeStarted(ActionMode mode) {
    }

    public void onSupportActionModeFinished(ActionMode mode) {
    }

    public ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback) {
        return null;
    }

    public void changeRes() {
        attrsViewUtils.changeRes();
    }
}
