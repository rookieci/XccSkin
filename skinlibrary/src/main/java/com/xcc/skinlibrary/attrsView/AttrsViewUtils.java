package com.xcc.skinlibrary.attrsView;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.util.ArrayMap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xcc.skinlibrary.Logger;
import com.xcc.skinlibrary.ResourcesManager;
import com.xcc.skinlibrary.SkinManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 52463 on 2018/6/28.
 */
public class AttrsViewUtils {
    public AttrsViewUtils(Activity activity) {
        //this.activity = activity;
        resources = activity.getResources();
        attributeSetMap = new ArrayMap<>();
        viewList = new ArrayList<>();
    }

    private Resources resources;
    private List<View> viewList;
    private Map<View, AttrSet> attributeSetMap;
    private ResourcesManager resourcesManager;

    public void addViewAttrs(View view, AttributeSet attrs) {
        int attributeCount = attrs.getAttributeCount();
        AttrSet attrSet = new AttrSet();
        for (int i = 0; i < attributeCount; i++) {
            String attributeName = attrs.getAttributeName(i);
            String value = attrs.getAttributeValue(i);
            attrSet.setAttribute(attributeName, value);
        }
        viewList.add(view);
        attributeSetMap.put(view, attrSet);
        changeRes(view, attrSet);
    }

    //更改所有View的res
    public void changeRes() {
        int size = viewList.size();
        View view;
        AttrSet attrs;
        for (int i = 0; i < size; i++) {
            view = viewList.get(i);
            attrs = attributeSetMap.get(view);
            changeRes(view, attrs);
        }
    }

    //更改当前View的res
    private void changeRes(View view, AttrSet attrs) {
        resourcesManager = SkinManager.getInstance().getResourcesManager();
        //Logger.out("---->layout>" + name);
        int attributeCount = attrs.getAttributeCount();
        for (int i = 0; i < attributeCount; i++) {
            String attributeName = attrs.getAttributeName(i);
            String value = attrs.getAttributeValue(i);
            Logger.d("-->" + attributeName, value);
            if (value.charAt(0) == '@') {
                String substring = value.substring(1, value.length());
                int id = Integer.parseInt(substring);
                if ("textColor".equals(attributeName)) {
                    textColorAttrs(view, id);
                } else if ("src".equals(attributeName)) {
                    srcAttrs(view, id);
                } else if ("background".equals(attributeName)) {
                    backgroundAttrs(view, id);
                } else if ("drawableLeft".equals(attributeName)) {
                    compoundDrawablesAttrs(view, id, 0);
                } else if ("drawableTop".equals(attributeName)) {
                    compoundDrawablesAttrs(view, id, 1);
                } else if ("drawableRight".equals(attributeName)) {
                    compoundDrawablesAttrs(view, id, 2);
                } else if ("drawableBottom".equals(attributeName)) {
                    compoundDrawablesAttrs(view, id, 3);
                }
            }
            //else if (value.charAt(0) == '?') {
            //D/-->style: ?attr/toolbarStyle
            //}
        }
    }

    private void textColorAttrs(View view, int id) {
        //String resourceName = resources.getResourceName(id);
        //String typeName = resources.getResourceTypeName(id);
        String name = resources.getResourceEntryName(id);
        ColorStateList colorStateList = null;
        if (resourcesManager != null) {
            colorStateList = resourcesManager.getColorByName(name);
        }
        if (colorStateList == null) colorStateList = resources.getColorStateList(id);
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            textView.setTextColor(colorStateList);
        }
    }

    private void srcAttrs(View view, int id) {
        String typeName = resources.getResourceTypeName(id);
        String name = resources.getResourceEntryName(id);
        Drawable drawable = null;
        if (resourcesManager != null) {
            drawable = resourcesManager.getDrawableByName(typeName, name);
        }
        if (drawable == null) drawable = resources.getDrawable(id);
        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            imageView.setImageDrawable(drawable);
        }
    }

    private void backgroundAttrs(View view, int id) {
        String typeName = resources.getResourceTypeName(id);
        String name = resources.getResourceEntryName(id);
        Drawable drawable = null;
        if (resourcesManager != null) {
            drawable = resourcesManager.getDrawableByName(typeName, name);
        }
        if (drawable == null) drawable = resources.getDrawable(id);
        view.setBackground(drawable);
    }

    /**
     * drawableLeft
     * drawableTop
     * drawableRight
     * drawableBottom
     */
    private void compoundDrawablesAttrs(View view, int id, int idx) {
        String typeName = resources.getResourceTypeName(id);
        String name = resources.getResourceEntryName(id);
        Drawable drawable = null;
        if (resourcesManager != null) {
            drawable = resourcesManager.getDrawableByName(typeName, name);
        }
        if (drawable == null) drawable = resources.getDrawable(id);
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            Drawable[] drawables = textView.getCompoundDrawablesRelative();
            drawables[idx] = drawable;
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawables[0], drawables[1], drawables[2], drawables[3]);
        }
    }
}
