package com.xcc.skinlibrary.attrsView;

import android.support.v4.util.ArrayMap;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 52463 on 2018/6/29.
 * AttributeSet 是一次性的，所以得用这个缓存一次
 */
public class AttrSet {
    private Map<String, String> map = new ArrayMap<>();
    private List<String> list = new ArrayList<>();

    public int getAttributeCount() {
        return map.size();
    }

    public void setAttribute(String name, String value) {
        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(value)) return;
        list.add(name);
        map.put(name, value);
    }

    public String getAttributeName(int i) {
        return list.get(i);
    }

    public String getAttributeValue(int i) {
        String s = list.get(i);
        return map.get(s);
    }
}
