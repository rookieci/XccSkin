package com.xcc.skinlibrary;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by 52463 on 2018/3/13.
 */

public class SkinConfig {
    private static final String Setting = "SkinConfig";


    public static void setPath(Context context, String pluginApkPath) {
        SharedPreferences sp = context.getSharedPreferences(Setting, 0);
        sp.edit().putString("pluginApkPath", pluginApkPath).apply();
    }

    public static String getPath(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Setting, 0);
        return sp.getString("pluginApkPath", "");
    }
}
