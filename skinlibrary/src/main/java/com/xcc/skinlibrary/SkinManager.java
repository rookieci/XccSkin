package com.xcc.skinlibrary;

import android.app.Activity;
import android.content.Context;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.LayoutInflaterCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;

import com.xcc.skinlibrary.inflater.LayoutInflaterFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 52463 on 2018/6/27.
 */

public class SkinManager implements OnLoadPluginListener {
    public static synchronized SkinManager getInstance() {
        if (skinManager == null) skinManager = new SkinManager();
        return skinManager;
    }

    private SkinManager() {
        factoryMap = new ArrayMap<>();
        activityList = new ArrayList<>();
    }

    private static SkinManager skinManager;
    public static final String pluginApkPackageName = "com.xcc.skin";//插件Apk的包名
    private ResourcesManager resourcesManager;
    private Map<Activity, LayoutInflaterFactory> factoryMap;
    private List<Activity> activityList;
    private OnLoadPluginListener onLoadPluginListener;

    public SkinManager setOnLoadPluginListener(OnLoadPluginListener onLoadPluginListener) {
        this.onLoadPluginListener = onLoadPluginListener;
        return this;
    }

    public void initRes(Context context) {
        String path = SkinConfig.getPath(context);
        if (!TextUtils.isEmpty(path)) changeRes(context, path);
    }

    /**
     * 初始化或更换皮肤资源 {@link ResourcesManager}
     *
     * @param context
     * @param pluginApkPath 文件路径 /sdcard/xxxx/xxxx.apk
     */
    public void changeRes(Context context, String pluginApkPath) {
        SkinConfig.setPath(context, pluginApkPath);
        new LoadPlugin(context, pluginApkPath).setOnLoadPluginListener(this).start();
    }

    /**
     * 使用App默认资源
     */
    public void changeDefRes(Context context) {
        resourcesManager = null;
        SkinConfig.setPath(context, "");
        // 发送更换事件
        changViewSkin();
    }

    public void onLoad(ResourcesManager resourcesManager) {
        this.resourcesManager = resourcesManager;
        if (onLoadPluginListener != null) onLoadPluginListener.onLoad(resourcesManager);
        // 发送更换事件
        changViewSkin();
    }

    public ResourcesManager getResourcesManager() {
        return resourcesManager;
    }

    public void registerSkin(Activity activity) {
        LayoutInflaterFactory factory = new LayoutInflaterFactory(activity);
        LayoutInflater inflater = LayoutInflater.from(activity);
        LayoutInflaterCompat.setFactory2(inflater, factory);
        factoryMap.put(activity, factory);
        activityList.add(activity);
    }

    public void unRegisterSkin(Activity activity) {
        factoryMap.remove(activity);
        activityList.remove(activity);
    }

    private void changViewSkin() {
        int size = activityList.size();
        Activity activity;
        LayoutInflaterFactory factory;
        for (int i = 0; i < size; i++) {
            activity = activityList.get(i);
            factory = factoryMap.get(activity);
            factory.changeRes();
        }
    }
}
