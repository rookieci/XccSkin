# XccSkin

#### 项目介绍
旧的换肤工具用不了，那就撸一个新的吧
前两年的时候，写了一个换肤的dome，最近翻出来感觉不好用，那自己就动手写个好用的

#### 使用说明

可以直接下载源码。没有制作引用包，所以使用 compile 'xxxxx' 来使用这个是不行的。
1. 将skinlibrary放进项目，然后compile project(':skinlibrary')一下就可以了；
2. 创建一个module作为皮肤包，包名必须是com.xcc.skin，可以在[SkinManager.java](https://gitee.com/rookieci/XccSkin/blob/master/skinlibrary/src/main/java/com/xcc/skinlibrary/SkinManager.java)文件中更改；
3. 换肤调用SkinManager.getInstance().setOnLoadPluginListener(this).changeRes(this, dataPath)。setOnLoadPluginListener用于加载皮肤资源是否成功回调。changeRes(this, dataPath)中，this是Activity，dataPath是皮肤资源的绝对路径，如果是储存卡路径需要用户自己动态获取权限。
3. 需要在Application中初始化皮肤，执行SkinManager.getInstance().initRes(this)。this是Application。
4. 如换背景图时，如果皮肤包中存在该资源，则使用该资源，否则仍会使用原先app中的资源。换肤原理是：获取资源类型和名称，以此去皮肤包中查找对一个的资源。所以要实现换肤，那皮肤包中资源名必须和原app中的资源名相同且类型也需要相同。
5. 换肤仅能针对xml中的view，即LayoutInflater加载xml创建的view。如：LayoutInflater.from(context).inflater(R.layout.item_list,null)或setContentView(R.layout.activity_main)。直接View view=new View(context)或setContentView(view)未实现换肤。原理可查看[LayoutInflaterFactory.java](https://gitee.com/rookieci/XccSkin/blob/master/skinlibrary/src/main/java/com/xcc/skinlibrary/inflater/LayoutInflaterFactory.java)文件或研究LayoutInflater.Factory2(android.view.LayoutInflater.Factory2)原理。

#### 运行图片
1. 换肤前
![换肤前](https://gitee.com/uploads/images/2018/0629/214722_73dd3717_1041409.png "截屏_20180629_212224.png")
2. 换肤后
![换肤后](https://gitee.com/uploads/images/2018/0629/214743_7178914d_1041409.png "截屏_20180629_212231.png")
3. gif运行效果（大小5M）
图片过大，不能直接预览，[点击此处预览](https://gitee.com/rookieci/XccSkin/blob/master/png/%E5%BD%95%E5%B1%8F_20180629_212338.gif)